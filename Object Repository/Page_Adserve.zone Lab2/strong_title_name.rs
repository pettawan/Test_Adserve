<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_title_name</name>
   <tag></tag>
   <elementGuidId>5b4e96ca-6359-42a4-9339-54a7e7d3b2f1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[1]/div[@class=&quot;page-header&quot;]/div[@class=&quot;page-top&quot;]/div[@class=&quot;right logo&quot;]/div[@class=&quot;right&quot;]/strong[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>tony@logicton.com</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;page-header&quot;]/div[@class=&quot;page-top&quot;]/div[@class=&quot;right logo&quot;]/div[@class=&quot;right&quot;]/strong[1]</value>
   </webElementProperties>
</WebElementEntity>
