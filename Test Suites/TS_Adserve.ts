<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_Adserve</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-03-09T20:28:55</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1b274269-425d-458f-b665-0862e454750f</testSuiteGuid>
   <testCaseLink>
      <guid>7548696c-8518-456a-97c9-e1ae346f8daf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b134d6e-6a4e-4144-975f-84601dd246ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Normal_Placement_Check_TestButton</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dfb9cc44-389e-46c6-b48a-3ea83b8a6a5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Normal_Placement_Check_Impressions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed17fd6c-5a09-4beb-8f30-0fb96135e1bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Normal_Placement_Check_Count_Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>badeea73-b73e-43b8-8198-55255a2b7115</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Normal_Placement_Check_Count_Banner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6326bd8f-da53-4499-beae-79d944329867</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Special_Check_TestButton</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bd5677e-a4bb-4448-bba6-d795d1e87ae6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Specia_Check_Count_Banner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56163311-be66-4805-8d99-ea81d4783688</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Specia_Check_Both_Modes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85e9ba18-c12b-41a8-af2f-26e7a5fa7ce1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Specia_Check_Save_Special_Placement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a029f3f2-be8c-4360-8a57-4f5b0e525290</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Response_And_Load_Time</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67173439-a8c4-49b2-a968-70f8cf9a0583</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_FTP_Connection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a849cc94-0d0b-48fb-aa30-afdef41d7d68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TC_Check_Database_Connection</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
