import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver

import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String username = 'artworx3dbadm'
String password = 'sVsAuAYK5qaYUMYK'
String database = 'Database\nadserve_awx'

'Open Browser'
WebUI.openBrowser('')
WebUI.maximizeWindow()

'Navigate To Url'
WebUI.navigateToUrl('http://artworx3phpmyadmin:p6dEb8PY4qKuJAWG@lab2.adserve.zone/phpmyadmin')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Page_DB/from_login'), 10, FailureHandling.STOP_ON_FAILURE)
WebUI.verifyElementVisible(findTestObject('Page_DB/input_username'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(1)
WebUI.setText(findTestObject('Page_DB/input_username'), username)
WebUI.delay(1)
WebUI.setText(findTestObject('Page_DB/input_password'), password)
WebUI.delay(1)
WebUI.click(findTestObject('Page_DB/button_go'))
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebDriver driver = DriverFactory.getWebDriver()
driver.switchTo().frame(driver.findElement(By.id('frame_content')))
WebUI.click(findTestObject('Page_artworx3.vps.ip.no/a_Databases'))
WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Page_DB/table_databases'), 10, FailureHandling.STOP_ON_FAILURE)
checkTb = WebUI.getText(findTestObject('Page_DB/table_databases'))
println 'checkTb ====='+checkTb
String[] result = checkTb.split(" ")
println 'result1====='+result[0]
WebUI.verifyMatch(result[0].toString(), database, false)

'Close Browser'
WebUI.closeBrowser()