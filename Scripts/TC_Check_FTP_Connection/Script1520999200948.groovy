import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String header = 'Index of /'
String tbody = '.bash_history'

'Open Browser'
WebUI.openBrowser('')
WebUI.maximizeWindow()

'Navigate To Url'
WebUI.navigateToUrl('ftp://artworx:d2tTRd3MMCSwysw5@lab2.adserve.zone')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebUI.waitForElementVisible(findTestObject('Page_FTP_Lab2/h1_header'), 10, FailureHandling.STOP_ON_FAILURE)
WebUI.verifyElementVisible(findTestObject('Page_FTP_Lab2/h1_header'), FailureHandling.STOP_ON_FAILURE)
checkHeader = WebUI.getText(findTestObject('Page_FTP_Lab2/h1_header'))
println 'checkHeader ======'+checkHeader
WebUI.verifyMatch(checkHeader, header, false)

WebUI.waitForElementVisible(findTestObject('Page_FTP_Lab2/tbody'), 10, FailureHandling.STOP_ON_FAILURE)
WebUI.verifyElementVisible(findTestObject('Page_FTP_Lab2/tbody'), FailureHandling.STOP_ON_FAILURE)
checkTbody = WebUI.getText(findTestObject('Page_FTP_Lab2/tbody'))
String[] result = checkTbody.split(" ")
println 'result====='+result[0]
println 'checkTbody ======'+checkTbody
WebUI.verifyMatch(result[0].toString(), tbody, false)

'Close Browser'
WebUI.closeBrowser()