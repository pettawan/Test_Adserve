import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import java.util.regex.Pattern

String username = 'tony@logicton.com'

String passwd = 'logicton2017!'

'Open Browser'
WebUI.openBrowser('')
WebUI.setViewPortSize(1920, 1800)

'Navigate To Url'
WebUI.navigateToUrl('http://lab2.adserve.zone/feelthefreedom')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2/input_username'), FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_username'), username)
WebUI.delay(1)

WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_passwd'), passwd)
WebUI.delay(1)

WebUI.click(findTestObject('Page_Adserve.zone Lab2/input_Submit'))
WebUI.delay(5)

WebUI.navigateToUrl('http://lab2.adserve.zone/adsawx/campaigns/summary.php?campaign_id=9')
WebUI.waitForPageLoad(10)
WebUI.delay(8)

// check campaign before list result
WebDriver driver = DriverFactory.getWebDriver()
WebUI.waitForElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/table'), 20, FailureHandling.STOP_ON_FAILURE)
WebElement Table = driver.findElement(By.id('table'))
List<WebElement> rowsTable = Table.findElements(By.tagName('tr'))
int rowsCount = rowsTable.size()
int row
String getName2 = ''
String foundList = 'N'
String name = 'War Dog Test Placement'
String impressionsBefore = ''
String clicksBefore = ''

Loop: for (row = 0; row < rowsCount; row++) {
    List<WebElement> columnsRow = rowsTable.get(row).findElements(By.tagName('td'))
    getName2 = columnsRow.get(0).getText()
	String[] result = getName2.split("\n")
    if (result[0].equals(name)) {
		foundList = 'Y'
		impressionsBefore = columnsRow.get(4).getText()
		clicksBefore = columnsRow.get(5).getText()
		columnsRow.get(11).findElement(By.linkText('TEST')).click()
		println('====== IN')
		WebUI.delay(2)
		Loop: break
	}
}

WebUI.verifyMatch(foundList, 'Y', false)
println "impressionsBefore =====:"+impressionsBefore
println "clicksBefore =====:"+clicksBefore
String textImpressionsBefore = impressionsBefore.toString()

String[] getTextimpressionsBefore = textImpressionsBefore.split("\\(")
String[] resultImpressionsBefore = getTextimpressionsBefore[0].split(" ")

int strResultImpressionsBefore = resultImpressionsBefore.length
String textResultImpressionsBefore = ""
for(int i = 0; i < strResultImpressionsBefore; i++){
	textResultImpressionsBefore += resultImpressionsBefore[i].toString()
}
println "textResultImpressionsBefore =====:"+textResultImpressionsBefore

text1IntimpressionsBefore = Integer.parseInt(textResultImpressionsBefore)

int numberImpressions = 3
int sumImpressionsBefore  = text1IntimpressionsBefore+numberImpressions
println 'sumImpressionsBefore :===='+sumImpressionsBefore

WebUI.delay(5)
WebUI.switchToWindowIndex(1)
WebUI.delay(8)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement Te/input_test_r1'), FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Page_Adserve.zone Lab2 Placement Te/input_test_r1'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement Te/input_test_r2'), FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Page_Adserve.zone Lab2 Placement Te/input_test_r2'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(3)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement Te/img'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(2)

WebUI.switchToWindowIndex(0)
WebUI.delay(2)

WebUI.refresh()
WebUI.delay(8)

// check campaign after list result
WebDriver driveraf = DriverFactory.getWebDriver()
WebUI.waitForElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/table'), 20, FailureHandling.STOP_ON_FAILURE)
WebElement Tableaf = driveraf.findElement(By.id('table'))
List<WebElement> rowsTableaf = Tableaf.findElements(By.tagName('tr'))
int rowsCountaf = rowsTableaf.size()
int rowaf = 0
String getNameaf = ''
String foundListaf = 'N'
String nameaf = 'War Dog Test Placement'
String impressionsAfter = ''
String clicksAfter = ''

Loop: for (rowaf = 0; rowaf < rowsCountaf; rowaf++) {
	List<WebElement> columnsRowaf = rowsTableaf.get(rowaf).findElements(By.tagName('td'))
	getNameaf2 = columnsRowaf.get(0).getText()
	String[] resultaf = getNameaf2.split("\n")
	if (resultaf[0].equals(nameaf)) {
		foundListaf = 'Y'
		impressionsAfter = columnsRowaf.get(4).getText()
		clicksAfter = columnsRowaf.get(5).getText()
		println('====== IN')
		WebUI.delay(2)
		Loop: break
	}
}
WebUI.verifyMatch(foundListaf, 'Y', false)
println "impressionsAfter =====:"+impressionsAfter
println "clicksAfter =====:"+clicksAfter
String textImpressionsAfter = impressionsAfter.toString()

String[] getTextimpressionsAfter = textImpressionsAfter.split("\\(")
String[] resultImpressionsAfter = getTextimpressionsAfter[0].split(" ")

int strResultImpressionsAfter = resultImpressionsBefore.length
String textResultImpressionsAfter = ""
for(int i = 0; i < strResultImpressionsAfter; i++){
	textResultImpressionsAfter += resultImpressionsAfter[i].toString()
}
println "textResultImpressionsAfter =====:"+textResultImpressionsAfter

text1IntimpressionsAfter = Integer.parseInt(textResultImpressionsAfter)

println 'text1IntimpressionsBefore :===='+text1IntimpressionsBefore
println 'text1IntimpressionsAfter :===='+text1IntimpressionsAfter

WebUI.verifyMatch(sumImpressionsBefore.toString(), text1IntimpressionsAfter.toString(), false)
WebUI.delay(2)

'Close Browser'
WebUI.closeBrowser()
