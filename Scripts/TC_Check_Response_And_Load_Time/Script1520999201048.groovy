import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver

String username = 'tony@logicton.com'
String passwd = 'logicton2017!'
String checkTime = 'N'

'Open Browser'
WebUI.openBrowser('')
WebUI.maximizeWindow()

long startTime = System.currentTimeMillis()

'Navigate To Url'
WebUI.navigateToUrl('http://lab2.adserve.zone/feelthefreedom')
WebUI.waitForPageLoad(10)

long endTime = System.currentTimeMillis()
long totalTime = (endTime - startTime)/1000

println 'Total Page Load Time: ' + totalTime + 'milliseconds'

if(totalTime <=10){
	checkTime = 'Y'
	println 'Total Page Load Time: ' + totalTime + 'milliseconds'
}
WebUI.verifyMatch(checkTime, 'Y', false)

'Close Browser'
WebUI.closeBrowser()
