import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String username = 'tony@logicton.com'
String passwd = 'logicton2017!'

'Open Browser'
WebUI.openBrowser('')
WebUI.maximizeWindow()

'Navigate To Url'
WebUI.navigateToUrl('http://lab2.adserve.zone/feelthefreedom')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2/input_username'), FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_username'), username)
WebUI.delay(1)
WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_passwd'), passwd)
WebUI.delay(1)
WebUI.click(findTestObject('Page_Adserve.zone Lab2/input_Submit'))
WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2/strong_title_name'), FailureHandling.STOP_ON_FAILURE)
checktitle = WebUI.getText(findTestObject('Page_Adserve.zone Lab2/strong_title_name'))
WebUI.verifyEqual(checktitle, username, FailureHandling.STOP_ON_FAILURE)
WebUI.delay(1)

'Close Browser'
WebUI.closeBrowser()
