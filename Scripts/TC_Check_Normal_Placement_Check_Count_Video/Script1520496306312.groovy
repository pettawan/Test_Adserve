import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable
import java.util.regex.Pattern

String username = 'tony@logicton.com'
String passwd = 'logicton2017!'

'Open Browser'
WebUI.openBrowser('')
WebUI.setViewPortSize(1920, 1800)

'Navigate To Url'
WebUI.navigateToUrl('http://lab2.adserve.zone/feelthefreedom')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2/input_username'), FailureHandling.STOP_ON_FAILURE)
WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_username'), username)
WebUI.delay(1)

WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_passwd'), passwd)
WebUI.delay(1)

WebUI.click(findTestObject('Page_Adserve.zone Lab2/input_Submit'))
WebUI.delay(5)

WebUI.navigateToUrl('http://lab2.adserve.zone/adsawx/campaigns/summary.php?campaign_id=9')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

// check campaign before list result
WebDriver driver = DriverFactory.getWebDriver()
WebUI.waitForElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/table'), 20, FailureHandling.STOP_ON_FAILURE)
WebElement Table = driver.findElement(By.id('table'))
List<WebElement> rowsTable = Table.findElements(By.tagName('tr'))
int rowsCount = rowsTable.size()
int row
String getName2 = ''
String foundList = 'N'
String name = 'War Dog Test Placement'
String impressionsBefore = ''
String clicksBefore = ''

Loop: for (row = 0; row < rowsCount; row++) {
    List<WebElement> columnsRow = rowsTable.get(row).findElements(By.tagName('td'))
	getName2 = columnsRow.get(0).getText()
	String[] result = getName2.split("\n")
	if (result[0].equals(name)) {
		foundList = 'Y'
		clicksBefore = columnsRow.get(5).getText()
		columnsRow.get(11).findElement(By.linkText('TEST')).click()
		println('====== IN')
		WebUI.delay(2)
		Loop: break
	}
}
WebUI.verifyMatch(foundList, 'Y', false)
println "clicksBefore =====:"+clicksBefore
int text1IntclicksBefore = 0
if (Pattern.compile(" ").matcher(clicksBefore).find()) {
	String[] subStr = clicksBefore.split(" ")
	int strSize = subStr.length
	String textclicksBefore = ""
	for(int i = 0; i < strSize; i++){
		textclicksBefore += subStr[i].toString()
	}
	println "textclicksBefore =====:"+textclicksBefore
	text1IntclicksBefore = Integer.parseInt(textclicksBefore)
} else {
	text1IntclicksBefore = Integer.parseInt(clicksBefore)
}
int numberclicksBefore = 1
int sumclicksBeforee = text1IntclicksBefore+numberclicksBefore
println 'text1IntclicksBefore :===='+text1IntclicksBefore

WebUI.delay(5)
WebUI.switchToWindowIndex(1)
WebUI.delay(8)
driver.switchTo().frame(driver.findElement(By.xpath('/html/body/div[2]/div/div/iframe')))
driver.findElement(By.id('clickArea')).click()
WebUI.delay(5)

WebUI.switchToWindowIndex(0)
WebUI.delay(2)

WebUI.refresh()
WebUI.delay(8)

// check campaign after list result
WebDriver driveraf = DriverFactory.getWebDriver()
WebUI.waitForElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/table'), 20, FailureHandling.STOP_ON_FAILURE)
WebElement Tableaf = driveraf.findElement(By.id('table'))
List<WebElement> rowsTableaf = Tableaf.findElements(By.tagName('tr'))
int rowsCountaf = rowsTableaf.size()
int rowaf = 0
String getNameaf = ''
String foundListaf = 'N'
String nameaf = 'War Dog Test Placement'
String impressionsAfter = ''
String clicksAfter = ''

Loop: for (rowaf = 0; rowaf < rowsCountaf; rowaf++) {
	List<WebElement> columnsRowaf = rowsTableaf.get(rowaf).findElements(By.tagName('td'))
	getNameaf2 = columnsRowaf.get(0).getText()
	String[] resultaf = getNameaf2.split("\n")
	if (resultaf[0].equals(nameaf)) {
		foundListaf = 'Y'
		clicksAfter = columnsRowaf.get(5).getText()
		println('====== IN')
		WebUI.delay(2)
		Loop: break
	}
}
WebUI.verifyMatch(foundListaf, 'Y', false)
println "clicksAfter =====:"+clicksAfter
int text1IntclicksAfter = 0
if (Pattern.compile(" ").matcher(clicksAfter).find()) {
	String[] subStr = clicksAfter.split(" ")
	int strSize = subStr.length
	String textclicksAfter = ""
	for(int i = 0; i < strSize; i++){
		textclicksAfter += subStr[i].toString()
	}
	println "textclicksAfter =====:"+textclicksAfter
	text1IntclicksAfter = Integer.parseInt(textclicksAfter)
} else {
	text1IntclicksAfter = Integer.parseInt(clicksAfter)
}
println 'text1IntclicksBefore :===='+text1IntclicksBefore
println 'text1IntclicksAfter :===='+text1IntclicksAfter

WebUI.verifyMatch(sumclicksBeforee.toString(), text1IntclicksAfter.toString(), false)
WebUI.delay(2)

'Close Browser'
WebUI.closeBrowser()