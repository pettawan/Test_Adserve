import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

String username = 'tony@logicton.com'

String passwd = 'logicton2017!'

String checkDes = 'Autoplay/Show Preview video on mouseOver'

'Open Browser'
WebUI.openBrowser('')
WebUI.setViewPortSize(1920, 1800)

'Navigate To Url'
WebUI.navigateToUrl('http://lab2.adserve.zone/feelthefreedom')
WebUI.waitForPageLoad(10)
WebUI.delay(5)

WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2/input_username'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_username'), username)
WebUI.delay(1)

WebUI.setText(findTestObject('Page_Adserve.zone Lab2/input_passwd'), passwd)
WebUI.delay(1)

WebUI.click(findTestObject('Page_Adserve.zone Lab2/input_Submit'))
WebUI.delay(5)

WebUI.navigateToUrl('http://lab2.adserve.zone/adsawx/campaigns/summary.php?campaign_id=13')
WebUI.waitForPageLoad(10)
WebUI.delay(8)

WebUI.waitForElementVisible(findTestObject('Page_Adserve.zone Lab2/button_tab_sp'), 20, FailureHandling.STOP_ON_FAILURE)
WebUI.click(findTestObject('Page_Adserve.zone Lab2/button_tab_sp'), FailureHandling.STOP_ON_FAILURE)
WebUI.waitForPageLoad(10)
WebUI.delay(8)

// check gateway list result
WebDriver driver = DriverFactory.getWebDriver()
WebUI.waitForElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/table'), 20, FailureHandling.STOP_ON_FAILURE)
WebElement Table = driver.findElement(By.id('table'))
List<WebElement> rowsTable = Table.findElements(By.tagName('tr'))
int rowsCount = rowsTable.size()
int row
String getName = ''
String getName2 = ''
String foundList = 'N'
String name = 'Test Special 001'

Loop: for (row = 0; row < rowsCount; row++) {
    List<WebElement> columnsRow = rowsTable.get(row).findElements(By.tagName('td'))
    getName2 = columnsRow.get(0).getText()
	String[] result = getName2.split("\n")
	if (result[0].equals(name)) {
		foundList = 'Y'
		columnsRow.get(10).findElement(By.linkText('TEST')).click()
		println('====== IN')
		WebUI.delay(2)
		Loop: break
	}
}
WebUI.verifyMatch(foundList, 'Y', false)

WebUI.delay(5)
WebUI.switchToWindowIndex(1)
WebUI.delay(5)

driver.switchTo().frame(driver.findElement(By.xpath('/html/body/div[2]/div/div[1]/iframe')));
WebUI.delay(3)
WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/div_clickArea'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(1)
WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/div_clickArea2'), FailureHandling.STOP_ON_FAILURE)
WebUI.delay(1)
WebUI.verifyElementVisible(findTestObject('Page_Adserve.zone Lab2 Placement/div_description'), FailureHandling.STOP_ON_FAILURE)
checkdescription = WebUI.getText(findTestObject('Page_Adserve.zone Lab2 Placement/div_description'))
WebUI.verifyEqual(checkdescription, checkDes, FailureHandling.STOP_ON_FAILURE)
WebUI.delay(1)

'Close Browser'
WebUI.closeBrowser()
